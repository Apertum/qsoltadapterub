/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.ubsolt;

import javax.swing.table.AbstractTableModel;
import static ru.apertum.qsystem.common.CustomerState.STATE_INVITED;
import static ru.apertum.qsystem.common.CustomerState.STATE_INVITED_SECONDARY;
import static ru.apertum.qsystem.common.CustomerState.STATE_WORK;
import static ru.apertum.qsystem.common.CustomerState.STATE_WORK_SECONDARY;

/**
 *
 * @author Evgeniy Egorov
 */
public class SoltUserTableModel extends AbstractTableModel {

    private final SoltAddrProp props;

    SoltUserTableModel(SoltAddrProp instance) {
        props = instance;
    }

    @Override
    public int getRowCount() {
        return props.getAddrs().size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Long userId = props.getAddrs().keySet().toArray(new Long[0])[rowIndex];
        switch (columnIndex) {
            case 0:
                return props.getDevice(userId);
            case 1:
                return props.getDevice(userId).addres;
            case 2:
                return props.getDevice(userId).serveceName == null ? (props.getDevice(userId).redirectServiceId == null || props.getDevice(userId).redirectServiceId == 0 ? "нет" : props.getDevice(userId).redirectServiceId.toString()) : props.getDevice(userId).serveceName;
            case 3:
                return props.getDevice(userId).qsize;
            case 4:
                if (props.getDevice(userId).getUser() == null) {
                    return "Неизвестно";
                }
                if (props.getDevice(userId).getUser().getShadow() == null || props.getDevice(userId).getUser().getShadow().getCustomerState() == null) {
                    return "Не работает";
                } else {
                    switch (props.getDevice(userId).getUser().getShadow().getCustomerState()) {
                        case STATE_INVITED:
                            return "Вызван";
                        case STATE_INVITED_SECONDARY:
                            return "Вызван по этапу";
                        case STATE_WORK:
                            return "В работе";
                        case STATE_WORK_SECONDARY:
                            return "В работе по этапу";
                        case STATE_DEAD:
                            return "Откланен";
                        case STATE_FINISH:
                            return "Закончил работу";
                        case STATE_BACK:
                            return "Возвращен";
                        case STATE_REDIRECT:
                            return "Перенаправлен";
                        case STATE_WAIT:
                            return "Ожидает";
                        case STATE_WAIT_AFTER_POSTPONED:
                            return "После отложения";
                        default:
                            throw new AssertionError();
                    }
                }
            default:
                throw new AssertionError();
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Оператор";
            case 1:
                return "ID RS485";
            case 2:
                return "Переадресация";
            case 3:
                return "Очередь";
            case 4:
                return "Состояние";
            default:
                throw new AssertionError();
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return ButtonSoltDevice.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return Integer.class;
            case 4:
                return String.class;
            default:
                throw new AssertionError();
        }
    }
}
