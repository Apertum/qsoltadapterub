/*
 * Copyright (C) 2014 Evgeniy Egorov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsystem.ubsolt;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import ru.apertum.qsystem.common.exceptions.ServerException;

/**
 *
 * @author Evgeniy Egorov
 */
public class Version {

    public static String ver = "";
    public static String date = "";

    public static void load() {
        final Properties settings = new Properties();
        final InputStream inStream = settings.getClass().getResourceAsStream("/ru/apertum/qsystem/ubsolt/ubsolt.properties");

        try {
            settings.load(inStream);
        } catch (IOException ex) {
            throw new ServerException("Cant read version. " + ex);
        }
        ver = settings.getProperty("version");
        date = settings.getProperty("date");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("============================================================");
        /*
        SoltAddrProp.getInstance().getAddrs().keySet().stream().forEach((id) -> {
            System.out.println("user: " + id + "  device: " + SoltAddrProp.getInstance().getAddrs().get(id).addres + "  redirect: " + SoltAddrProp.getInstance().getAddrs().get(id).redirect
                    + (SoltAddrProp.getInstance().getAddrs().get(id).redirect ? "  redirectServiceId=" + SoltAddrProp.getInstance().getAddrs().get(id).redirectServiceId : ""));
        });
        System.out.println("============================================================");
         */
        load();
        System.out.println("****** QSoltAdapterUb  ******");
        System.out.println("******    version " + ver + "    ******");
        System.out.println("****** date " + date + " ******");
        System.out.println("============================================================");
    }

}
