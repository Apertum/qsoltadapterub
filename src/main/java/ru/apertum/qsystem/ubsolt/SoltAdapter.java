/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.ubsolt;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import ru.apertum.qsystem.common.cmd.RpcGetServerState;
import ru.apertum.qsystem.extra.IButtonDeviceFuctory;
import ru.apertum.qsystem.server.model.QPlanService;
import ru.apertum.qsystem.server.model.QUser;

/**
 *
 * @author Evgeniy Egorov
 */
public class SoltAdapter implements IButtonDeviceFuctory {

    @Override
    public String getDescription() {
        return "SOLT protocol adapter.";
    }

    @Override
    public long getUID() {
        return 109L;
    }

    @Override
    public IButtonDevice getButtonDevice(byte[] bytes, List<QUser> users) {
        setUsers(users);
        byte[] t = Arrays.copyOfRange(bytes, 4, 10);
        String key = new String(t);
        return SoltAddrProp.getInstance().getDevrByAddr(key, users);
    }

    @Override
    public AbstractTableModel getDeviceTable() {
        return new SoltUserTableModel(SoltAddrProp.getInstance());
    }

    @Override
    public LinkedList<IButtonDevice> getButtonDevices(List<QUser> users) {
        setUsers(users);
        return SoltAddrProp.getInstance().getDevs();
    }

    private void setUsers(List<QUser> users) {
        users.forEach(user -> {
            IButtonDevice dev = SoltAddrProp.getInstance().getDevice(user.getId());
            if (dev != null) {
                dev.setUser(user);
            }
        });
    }

    @Override
    public void refreshDeviceTable(LinkedList<QUser> users, LinkedList<RpcGetServerState.ServiceInfo> servs) {
        for (ButtonSoltDevice dev : SoltAddrProp.getInstance().getAddrs().values().toArray(new ButtonSoltDevice[0])) {
            for (QUser qUser : users) {
                if (dev.userId.equals(qUser.getId())) {
                    dev.setUser(qUser);
                    int l = 0;
                    for (QPlanService pser : qUser.getPlanServices()) {
                        for (RpcGetServerState.ServiceInfo serviceInfo : servs) {
                            if (serviceInfo.getId().equals(pser.getService().getId())) {
                                l = l + serviceInfo.getCountWait();
                            }
                        }
                    }
                    dev.qsize = l;
                    break;
                }
            }
            for (RpcGetServerState.ServiceInfo serviceInfo : servs) {
                if (dev.redirectServiceId != null && dev.redirectServiceId.equals(serviceInfo.getId())) {
                    dev.serveceName = serviceInfo.getServiceName();
                    break;
                }
            }
        }
    }

}
