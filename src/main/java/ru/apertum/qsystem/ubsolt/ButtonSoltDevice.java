/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.ubsolt;

import ru.apertum.qsystem.common.CustomerState;
import ru.apertum.qsystem.common.NetCommander;
import ru.apertum.qsystem.common.QLog;
import ru.apertum.qsystem.common.exceptions.QException;
import ru.apertum.qsystem.common.model.QCustomer;
import ru.apertum.qsystem.extra.IButtonDeviceFuctory.IButtonDevice;
import ru.apertum.qsystem.server.model.QUser;
import ru.apertum.qsystem.ub485.core.UBForm;

/**
 * Default buttons
 *
 * @author Evgeniy Egorov
 */
public class ButtonSoltDevice extends Object implements IButtonDevice {

    public final String addres;
    public final boolean redirect;
    public final Long redirectServiceId;
    public final Long userId;
    private QUser user;

    public Integer qsize = 0;

    public void setQsize(Integer qsize) {
        if ((user == null
                || user.getShadow() == null
                || user.getShadow().getCustomerState() == null
                || user.getShadow().getCustomerState() == CustomerState.STATE_BACK
                || user.getShadow().getCustomerState() == CustomerState.STATE_DEAD
                || user.getShadow().getCustomerState() == CustomerState.STATE_FINISH
                || user.getShadow().getCustomerState() == CustomerState.STATE_POSTPONED
                || user.getShadow().getCustomerState() == CustomerState.STATE_REDIRECT
                || user.getShadow().getCustomerState() == CustomerState.STATE_REDIRECT)
                && (this.qsize == 0 && qsize != 0)) {
        }
        this.qsize = qsize;
    }

    public String serveceName = null;

    public ButtonSoltDevice(Long userId, String addres, Long redirectServiceId) {
        this.addres = addres;
        this.userId = userId;
        this.redirectServiceId = redirectServiceId;
        this.redirect = redirectServiceId != null && redirectServiceId != 0;
    }

    @Override
    public String toString() {
        return user == null ? String.valueOf(userId) : user.getName();
    }

    /**
     * Тут вся логика работы кнопок и их нажатия
     *
     * @param bb это команда от устройства
     */
    @Override
    public void doAction(byte[] bb) {
        byte b = bb[1];
        if (b == 0x31) {
            System.out.println("b == 0x31 -- 49");
        } else if (b == 0x32) {
            System.out.println("b == 0x32 -- 50");
        } else if (b == 0x33) {
            System.out.println("b == 0x33 -- 51");
        } else if (b == 0x34) {
            System.out.println("b == 0x34 -- 52");
        } else {
            System.out.println("Button bot binded.....");
            return;
        }
        if (user == null) {
            System.out.println("user == null");
        } else {
            if (user.getShadow() == null) {
                System.out.println("user.getShadow() == null");
            } else {
                if (user.getShadow().getCustomerState() == null) {
                    System.out.println("user.getShadow().getCustomerState() == null");
                } else {
                    System.out.println("user.getShadow().getCustomerState() == " + user.getShadow().getCustomerState());
                }
            }
        }
        // первичный вызов
        if ((user.getShadow() == null
                || user.getShadow().getCustomerState() == null
                || user.getShadow().getCustomerState() == CustomerState.STATE_BACK
                || user.getShadow().getCustomerState() == CustomerState.STATE_DEAD
                || user.getShadow().getCustomerState() == CustomerState.STATE_FINISH
                || user.getShadow().getCustomerState() == CustomerState.STATE_POSTPONED
                || user.getShadow().getCustomerState() == CustomerState.STATE_REDIRECT)
                && (b == 0x31)) {
            //команда вызова кастомера
            System.out.println("Invite Next Customer by '" + user.getName() + "'");
            final QCustomer cust;
            try {
                cust = NetCommander.inviteNextCustomer(UBForm.getForm().getNetProperty(), userId);
            } catch (QException e) {
                QLog.l().logger().error("Error. Invite Next Customer by '" + user.getName() + "'", e);
                return;
            }
            System.out.println("inv ** 0");
            if (cust != null) {
                System.out.println("inv ** 1");
                if (user.getShadow() == null) {
                    user.setShadow(new QUser.Shadow(cust));
                    System.out.println("inv ** 1`");
                }
                user.getShadow().setCustomerState(cust.getState());
                System.out.println("inv ** 2");
                user.getShadow().setOldPref(cust.getPrefix());
                System.out.println("inv ** 3");
                user.getShadow().setOldNom(cust.getNumber());
                System.out.println("inv ** 4");

            } else {
                System.out.println("inv ** 5");
                user.getShadow().setCustomerState(CustomerState.STATE_FINISH);

            }
            System.out.println("inv ** 6");
            return;
        }

        // повторный вызов
        if ((user != null && user.getShadow() != null && user.getShadow().getCustomerState() != null)
                && (user.getShadow().getCustomerState() == CustomerState.STATE_INVITED || user.getShadow().getCustomerState() == CustomerState.STATE_INVITED_SECONDARY)
                && (b == 0x31)) {
            //команда вызова кастомера
            System.out.println("Recall by '" + user.getName() + "'");
            try {
                final QCustomer cust = NetCommander.inviteNextCustomer(UBForm.getForm().getNetProperty(), userId);
            } catch (QException e) {
                QLog.l().logger().error("Error. Recall by '" + user.getName() + "'", e);
                return;
            }
            System.out.println("--<>\n");
            return;
        }

        // начало работы
        if ((user != null && user.getShadow() != null && user.getShadow().getCustomerState() != null)
                && (user.getShadow().getCustomerState() == CustomerState.STATE_INVITED || user.getShadow().getCustomerState() == CustomerState.STATE_INVITED_SECONDARY)
                && (b == 0x32)) {
            //команда вызова кастомера
            System.out.println("get Start Customer " + userId);
            try {
                NetCommander.getStartCustomer(UBForm.getForm().getNetProperty(), userId);
            } catch (QException e) {
                QLog.l().logger().error("Error. get Start Customer '" + userId + "'", e);
                return;
            }
            System.out.println("--1\n");
            user.getShadow().setCustomerState(CustomerState.STATE_WORK);
            System.out.println("--2\n");

            return;
        }

        // отклонить по неявке
        if ((user != null && user.getShadow() != null && user.getShadow().getCustomerState() != null)
                && (user.getShadow().getCustomerState() == CustomerState.STATE_INVITED || user.getShadow().getCustomerState() == CustomerState.STATE_INVITED_SECONDARY)
                && (b == 0x34)) {
            //команда вызова кастомера
            System.out.println("kill Next Customer by '" + user.getName() + "'");
            try {
                NetCommander.killNextCustomer(UBForm.getForm().getNetProperty(), userId, user.getShadow().getIdOldCustomer());
            } catch (QException e) {
                QLog.l().logger().error("Error. kill Next Customer by '" + user.getName() + "'", e);
                return;
            }
            user.getShadow().setCustomerState(CustomerState.STATE_DEAD);

            return;
        }

        // закончить работу
        if ((user != null && user.getShadow() != null && user.getShadow().getCustomerState() != null)
                && (user.getShadow().getCustomerState() == CustomerState.STATE_WORK || user.getShadow().getCustomerState() == CustomerState.STATE_WORK_SECONDARY)
                && (b == 0x34)) {
            //команда завершения работы

            System.out.println("get Finish Customer by '" + user.getName() + "'");
            try {
                NetCommander.getFinishCustomer(UBForm.getForm().getNetProperty(), userId, user.getShadow().getIdOldCustomer(), -1L, "");
            } catch (QException e) {
                QLog.l().logger().error("Error. get Finish Customer by '" + user.getName() + "'", e);
                return;
            }
            user.getShadow().setCustomerState(CustomerState.STATE_FINISH);

            return;
        }

        //  перенаправить если есть куда
        if ((user != null && user.getShadow() != null && user.getShadow().getCustomerState() != null)
                && (user.getShadow().getCustomerState() == CustomerState.STATE_WORK || user.getShadow().getCustomerState() == CustomerState.STATE_WORK_SECONDARY)
                && (b == 0x33) && redirect) {
            //команда  редирект
            System.out.println("redirect Customer by '" + user.getName() + "'");
            try {
                NetCommander.redirectCustomer(UBForm.getForm().getNetProperty(), userId, user.getShadow().getIdOldCustomer(), redirectServiceId, false, "", -1L);
            } catch (QException e) {
                QLog.l().logger().error("Error. redirect Customer by '" + user.getName() + "'", e);
                return;
            }
            user.getShadow().setCustomerState(CustomerState.STATE_FINISH);
        }

    }

    @Override
    public void getFeedback() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeAdress() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void check() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public QUser getUser() {
        return user;
    }

    @Override
    public String getId() {
        return addres;
    }

    @Override
    public void setUser(QUser user) {
        this.user = user;
    }
}
