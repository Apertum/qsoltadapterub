/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.ubsolt;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import ru.apertum.qsystem.common.QLog;
import ru.apertum.qsystem.common.Uses;
import ru.apertum.qsystem.extra.IButtonDeviceFuctory;
import ru.apertum.qsystem.server.model.QUser;

/**
 *
 * @author Evgeniy Egorov
 */
public class SoltAddrProp {

    final private HashMap<Long, ButtonSoltDevice> addrs = new HashMap<>();
    final private LinkedList<IButtonDeviceFuctory.IButtonDevice> devs = new LinkedList<>();

    public LinkedList<IButtonDeviceFuctory.IButtonDevice> getDevs() {
        return devs;
    }

    public HashMap<Long, ButtonSoltDevice> getAddrs() {
        return addrs;
    }
    final static private File ADDR_FILE = new File("config/qubSolt.adr");

    private SoltAddrProp() {
        try (FileInputStream fis = new FileInputStream(ADDR_FILE); Scanner s = new Scanner(fis)) {
            while (s.hasNextLine()) {
                final String line = s.nextLine().trim();
                if (!line.isEmpty() && !line.startsWith("#")) {
                    final String[] ss = line.split("=");
                    if (ss.length != 2) {
                        QLog.l().logger().error(ADDR_FILE.getAbsolutePath() + " contaned an error, line: \"" + line + "\"");
                        continue;
                    }
                    final String[] ssl = ss[1].split(" ");
                    if (!(Uses.isInt(ss[0]) && (ssl.length == 2 ? Uses.isInt(ssl[1]) : true))) {
                        QLog.l().logger().error(ADDR_FILE.getAbsolutePath() + " contaned an error: \"" + line + "\" value \"" + Arrays.toString(ssl) + "\" is bad.");
                        continue;
                    }
                    ButtonSoltDevice dev = new ButtonSoltDevice(Long.valueOf(ss[0]), ssl[0], ssl.length == 1 ? null : Long.parseLong(ssl[1]));
                    addrs.put(Long.valueOf(ss[0]), dev);
                    devs.add(dev);
                }
            }
        } catch (IOException ex) {
            System.err.println(ex);
            throw new RuntimeException(ex);
        }
    }

    public static SoltAddrProp getInstance() {
        return AddrPropHolder.INSTANCE;
    }

    private static class AddrPropHolder {

        private static final SoltAddrProp INSTANCE = new SoltAddrProp();
    }

    public ButtonSoltDevice getDevice(Long userId) {
        return addrs.get(userId);
    }

    public ButtonSoltDevice getDevrByAddr(String rsAddr, List<QUser> users) {
        for (ButtonSoltDevice dev : SoltAddrProp.getInstance().getAddrs().values().toArray(new ButtonSoltDevice[0])) {
            if (dev.addres.equals(rsAddr)) {
                return dev;
            }
        }
        return null;
    }

    public static void main(String[] ss) {
        SoltAddrProp.getInstance().addrs.keySet().stream().forEach((id) -> {
            System.out.println(id + " addres: " + SoltAddrProp.getInstance().addrs.get(id).addres + " redirect: " + SoltAddrProp.getInstance().addrs.get(id).redirect);
        });

    }
}
